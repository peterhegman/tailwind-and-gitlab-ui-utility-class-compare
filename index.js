const postcss = require("postcss");
const fs = require("fs");
const { isEqual, sortBy } = require("lodash");
const { parseColor, formatColor } = require("tailwindcss/lib/util/color");

const run = async () => {
  const tailwind = fs.readFileSync("./output.css", "utf8");
  const response = await fetch(
    "https://unpkg.com/@gitlab/ui@72.8.1/dist/utility_classes.css"
  );
  const gitlabUi = await response.text();

  const parsedTailwind = [];
  const parsedGitlabUi = [];

  postcss.parse(tailwind).walkRules((rule) => {
    parsedTailwind.push({
      selector: rule.selector,
      nodes: rule.nodes.flatMap((node) => {
        if (node.prop.startsWith("--tw-")) {
          return [];
        }

        return [
          {
            prop: node.prop,
            value: node.value
              .replace(" / var(--tw-text-opacity)", "")
              .replace(" / var(--tw-bg-opacity)", "")
              .replace(" / var(--tw-border-opacity)", ""),
          },
        ];
      }),
    });
  });

  postcss.parse(gitlabUi).walkRules((rule) => {
    parsedGitlabUi.push({
      selector: rule.selector,
      nodes: rule.nodes.map((node) => {
        if (["color", "background-color", "border-color"].includes(node.prop)) {
          const parsedColor = parseColor(node.value);

          if (parsedColor) {
            return {
              prop: node.prop,
              value: formatColor(parsedColor),
            };
          }
        }

        return {
          prop: node.prop,
          value: node.value,
        };
      }),
    });
  });

  const matchedCssUtils = parsedGitlabUi
    .filter((util) => {
      const matchedTailwind = parsedTailwind.find((tailwindUtil) => {
        return tailwindUtil.selector === util.selector;
      });

      if (!matchedTailwind) {
        return false;
      }

      if (matchedTailwind.nodes.length !== util.nodes.length) {
        return false;
      }

      return isEqual(
        sortBy(matchedTailwind.nodes, ["prop"]),
        sortBy(util.nodes, ["prop"])
      );
    })
    .map((util) => util.selector);

  console.log(JSON.stringify(matchedCssUtils));
};

run();
