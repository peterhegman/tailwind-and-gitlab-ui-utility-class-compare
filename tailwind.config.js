const designTokens = require("./tokens.grouped.json");

const getColorTokens = () => {
  const colorTokens = {
    blue: {},
    gray: {},
    green: {},
    orange: {},
    purple: {},
    red: {},
  };

  Object.keys(colorTokens).forEach((palette) => {
    Object.keys(designTokens.color).forEach((key) => {
      if (new RegExp(`^${palette}-[0-9]{2,3}`).test(key)) {
        const [, shade] = key.split("-");
        colorTokens[palette][shade] = designTokens.color[key];
      }
    });
  });
  return colorTokens;
};

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  safelist: [
    {
      pattern: /./, // the "." means "everything"
    },
  ],
  theme: {
    extend: {},
    spacing: {
      1: ".125rem",
      2: ".25rem",
      3: ".5rem",
      4: ".75rem",
      5: "1rem",
      6: "1.5rem",
      7: "2rem",
      8: "2.5rem",
      9: "3rem",
      10: "3.5rem",
      11: "4rem",
      "11-5": "4.5rem",
      12: "5rem",
      13: "6rem",
      15: "7.5rem",
      20: "10rem",
      26: "13rem",
      28: "14rem",
      30: "15rem",
      31: "15.5rem",
      34: "17rem",
      48: "24rem",
      62: "31rem",
      75: "37.5rem",
      80: "40rem",
      88: "44rem",
    },
    colors: {
      black: designTokens.color.black,
      white: designTokens.color.white,
      ...getColorTokens(),
    },
  },
  plugins: [],
  prefix: "gl-",
};
